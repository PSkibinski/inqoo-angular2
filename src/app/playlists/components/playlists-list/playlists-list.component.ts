import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../model/model';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
})
export class PlaylistsListComponent implements OnInit {
  @Input('items') playlists: Playlist[] = [];

  @Input('selected') selected?: Playlist;

  @Output() selectedChange = new EventEmitter<Playlist>();

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist);
  }

  constructor() {}

  ngOnInit(): void {}
}
