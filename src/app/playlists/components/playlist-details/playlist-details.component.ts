import { Component, EventEmitter, Output, Input, OnInit } from '@angular/core';
import { Playlist } from '../../model/model';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss'],
})
export class PlaylistDetailsComponent implements OnInit {
  constructor() {}

  @Input('playlist') playlist!: Playlist;

  @Output() edit = new EventEmitter();

  editClick() {
    this.edit.emit();
  }

  ngOnInit(): void {}
}
