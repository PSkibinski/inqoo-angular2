import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Playlist } from '../../model/model';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss'],
})
export class PlaylistEditorComponent implements OnInit {
  draft!: Playlist;

  constructor() {}

  @Input('playlist') playlist: Playlist = {
    id: 'N/A',
    name: 'N/A',
    public: false,
    description: 'N/A',
  };

  @Output() cancel = new EventEmitter();

  @Output() save = new EventEmitter<Playlist>();

  cancelClick() {
    this.cancel.emit();
  }

  saveClick(draft: Playlist) {
    this.save.emit(draft);
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    this.draft = { ...this.playlist };
  }
}
