import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistEditorComponent } from './components/playlist-editor/playlist-editor.component';
import { PlaylistsRoutingModule } from './playlists-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PlaylistsViewComponent,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    PlaylistEditorComponent,
  ],
  imports: [CommonModule, PlaylistsRoutingModule, FormsModule],
  exports: [PlaylistsViewComponent],
})
export class PlaylistsModule {}
