import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
})
export class SearchFormComponent implements OnInit {
  constructor() {}

  albumTitle = '';

  @Output() search = new EventEmitter();
  @Output() searchByName = new EventEmitter();

  searchClick(query: string) {
    this.search.emit(query);
  }

  searchByNameClick(query: string) {
    this.searchByName.emit(query);
  }

  ngOnInit(): void {}
}
