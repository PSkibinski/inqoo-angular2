import { Component, Inject, OnInit, Output } from '@angular/core';
import { Album } from 'src/app/core/model/album';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss'],
})
export class AlbumSearchComponent implements OnInit {
  results: Album[] = [];

  constructor(
    // @Inject(AlbumSearchService)
    private service: AlbumSearchService
  ) {}

  search(query: string) {
    this.results = this.service.getAlbums();
  }

  searchByName(query: string) {
    this.results = this.service.getAlbumByName(query);
  }

  ngOnInit(): void {}
}
